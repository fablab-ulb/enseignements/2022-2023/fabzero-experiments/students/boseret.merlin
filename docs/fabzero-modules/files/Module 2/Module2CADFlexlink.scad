//LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

//...............Définition taille du rayon des trous conformément a la taille des tétons Lego et de différentes variables:
r=2.5;
$fn=500;

Largeurx=5;
epaisseur=5;

TigeX=1;
TigeY=65;

x1=0; y1=4; z1=0;
x2=0; y2=-4; z2=0;
x3=TigeX/2;  y3=6.5; z3=-epaisseur/2;

//...............Variable pour scale
Sx=1;
Sy=1;
Sz=1;

//...............Création du premier ovale qui contient la différence de deux cylindre de la taille des tenons Lego
module ovale(){
difference(){
    scale([Sx,2*Sy,Sz])cylinder(epaisseur, d = 1.5*Largeurx , center=true);
    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= 2*r, center=true);
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= 2*r, center=true);
}
}
//...............Création tige
translate([-x3,y3,z3]) cube([TigeX,TigeY,epaisseur]) ;

//...............Deuxième ovale (boucle)
for (i=[0:1]) {
  translate([0,i*78,0]) ovale();
}
