# Computer Controlled Cutting

## Introduction
L'objectif de ce module est de "prendre en main" les découpeuse laser, du disign à la découpe en passant par les consignes de sécurités. J'ai choisis ce module car j'ai toujours voulu savoir utiliser les découpeuses laser et que j'ai des idées de futur projet.3 découpeuses nous ont été présentés: la Epilog Fusion Pro 32, la Lasersaur et la Full Spectrum Muse. Elles ont toutes leur spécificités de pars leurs puissances différentes, surface de découpe vitesse etc...La Epilog est la plus facile d'utilisation,la lasersaure nécéssite qu'on fasse un focus avec le laser et qu'on allume le refroidisseur en ouvrant une vanne mais elle possède la plus grande surface de découpe, la Full Spectrum Muse nécéssite aussi un focus et l'ouverture d'une vanne pour l'extracteur de fumée et le refroidisseur a eau mais il est assez facile de s y connecter et elle est déplacable pour avoir une surface de découpe virtuellement infinie. Les deux paramètres principaux à prendre en compte lors d'une découpe sont la vitesse et la puissance. Une des premières chose a faire lorsque on veut faire une découpe (ou gravage) est un "test" comme celui ci, sur la machine que l'on vas utiliser :

![](images/testLaserCutter.JPG)

on peut voir sur les deux axes la vitesse et la puissance et on peut donc savoir lequelles utilisé dans l'optique d'une découpe gravure etc...Il est important de faire ce test sur la machine et le matériel que l'on vas utiliser.

## Mise en pratique
Mon partenaire et moi avons voullu découper une pièce d'échec sur du plexiglass de 3mm d'épaisseur avec la machine Full Spectrum Muse. Voici le fichier SVG que nous avons utilisé:

![](images/PhotoSVG.JPG)

La  Full Spectrum Muse est la machine qui a **le moin de puissance des 3**, elle a une puissance de 40W, la Lasersaure a une puissance de 100W et l'Epilog 60W. Pour cette raison nous avons été conseiller de mettre la puissance a 90% et la vitesse a 30% sur tout le tracé.Voici le résultat:

![](images/PhotoKnight.JPG)
![](images/photoPlexis.JPG)


## Assignement Kirigami

Un Kirigami est une structure en 3D issue de l'art du découpage, typiquement japonnais. Pour en réaliser il faut utiliser des découpes et des pliages. Sachant que ma matière première serra du papier,voir du papier cartonné la puissance de découpe serra très faible et la vitesse très rapide.J'ai donc disigner ceci:

![](images/Stsnsb%C3%A9zier.JPG)
![](images/StormtrooperAccourbes.JPG)

Les vecteurs rouge ne serront pas etre entièrement découper dans le papier, la vitesse serra donc de 100% et la puissance de 10%. Les vecteurs noires quant a eux doivent bel et bien être découper la vitesse serra donc de 100% et la puissance de 20%.


Mes vecteurs ne se dessinaient pas, je devais double cliquer sur le NA en bas a gauche de l'écran. Les lettres sont vectorisé car j ai séléctionner le texte, cliquer sur objet>objet chemin.La "languette qui part du dessus de la strucuture centrale doit faire la même taille que la distance qui sépare le milieu de la feuille et la base de la structure centrale.  La structure centrale revient à [Pavel Tytyuk](https://www.thingiverse.com/koshpavel/designs) qui est un utilisateur [Thingiverse](https://www.thingiverse.com/) dont je pouvais utiliser l'oeuvre du a son créative commons (CC BY-SA).
