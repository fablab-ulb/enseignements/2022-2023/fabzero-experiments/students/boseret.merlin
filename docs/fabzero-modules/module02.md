# 2. Conception Assistée par Ordinateur (CAO)

## Introduction

L'objectif de ce module est de produire une pièce [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks).Pour cela j'ai utilisé [openSCAD](https://openscad.org/).Une partie très importante pour la conception de la pièce était la bonne mesure des dimensions. J'ai utiliser un pied a coulisse électronique et une pièce Lego pour avoir les bonne mesures.

## Le code

Voici le code final:

```
//LICENSE : Creative Commons Attribution 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

//...............Définition taille du rayon des trous conformément a la taille des tétons Lego et de différentes variables:
r=2.5;
$fn=500;

Largeurx=5;
epaisseur=5;

TigeX=1;
TigeY=65;

x1=0; y1=4; z1=0;
x2=0; y2=-4; z2=0;
x3=TigeX/2;  y3=6.5; z3=-epaisseur/2;

//...............Variable pour scale
Sx=1;
Sy=1;
Sz=1;

//...............Création du premier ovale qui contient la différence de deux cylindre de la taille des tenons Lego
module ovale(){
difference(){
    scale([Sx,2*Sy,Sz])cylinder(epaisseur, d = 1.5*Largeurx , center=true);
    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= 2*r, center=true);
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= 2*r, center=true);
}
}
//...............Création tige
translate([-x3,y3,z3]) cube([TigeX,TigeY,epaisseur]) ;

//...............Deuxième ovale (boucle)
for (i=[0:1]) {
  translate([0,i*78,0]) ovale();
}

```
ce qui donne ceci:
![](images/Capture.JPG)




## Procédé

J'ai dabord trouvé que les "plateformes latérales" dans lesquelles allaient se trouvé les trous pouvaient être des cylindres étirés sur un seul axe (avec l'outil **scale**), faisant une sorte de ovale. 

```
scale([1,2,1]) cylinder(5, d = 2 , center=true)
```

(Dans l'exemple **ci-dessus** un ovale à été créé car lecylindre a été étirer d'un facteur 2 sur l'axe Y)

J'ai soustrait à ce ovale deux cylindre dont les mesures étaient celle d'un Lego (r=2.5) grace à l'outil **difference**.Les coordonnées de ces cylindres ont aussi du être minutieusement choisie en accord avec l'espacement entre les trous des Lego. J'ai ensuite créé une **"boucle de colnage"** pour obtenir mon deuxième ovale (j'aurais pus utiliser l'outil mirror).

```
for (i=[0:1]) {
  translate([0,i*80,0]) ovale();
}
```


(Dans l'exemple **ci-dessus** une boucle pour i allant de 0 a 1 a été créé. Le **module** "ovale" a donc été déplacé, grace a **translate** une fois de 80 unités en Y.Et ce, sans éffacer le module initial) 

J'ai ensuite créé une "tige" qui relie les 2.Un concepte très important que j'ai appris est la création de module (comme le module "ovale" dans mon code ).

```
module Cube(){
 cube (5)
}
```

(Dans l'exemple **ci-dessus** un module comprenant uniquement le cube de 5unités de côté a été créé)
 Cela m'a permis de créé une boucle qui clone **tout** le module (Ovale + différence des cylindres) et le déplace. La boucle que j'ai créé ne s'éffectue qu'une seule fois. 
La [CheatSheet](https://openscad.org/cheatsheet/)d'OpenScad et [ce site](https://edutechwiki.unige.ch/fr/Tutoriel_OpenSCAD#Cube)de EdutechWiki m'ont été très pratiques pour la théorie. J'ai mis mon code sous license Creative Commons.

## A améliorer

Les embouts latéraux n'ont pas la même forme que les FlexLinks et je crois pouvoir y parvenir avec les connaissances que j'ai maintenant. J'ai essayé que ce soit le plus paramétrique possible mais j ai l'impression que ce n'est pas tout a fait le cas. Je crois que l'idéal serrait que les deux seul paramètres changeable soit le diamètre des trous et la longueur de la pièce.





