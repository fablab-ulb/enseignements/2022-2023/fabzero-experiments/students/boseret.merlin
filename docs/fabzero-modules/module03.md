# 3. Impression 3D

## Introduction

L'objectif de ce module est d'imprimer la pièce [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks) précedament disigné sur [openSCAD](https://openscad.org/).
![](images/Capture.JPG).


## Imprimente

L'imprimante 3D que j ai utilisée est celle d'un ami, il s'agit de la Creality Ender 3.
J'ai donc utilisé le slicer [Ultimaker Cura](https://ultimaker.com/fr/software/ultimaker-cura) compatible avec cet imprimante pour obtenir mon fichier STL.
![](images/PhotoImprimente3dComprimax.jpg)


## Impressions
Après avoir importer mon fichier STL dans le slicer [Ultimaker Cura](https://ultimaker.com/fr/software/ultimaker-cura) pour le slicer et obtenir un fichier G-code (que l'imprimante "comprend" pour produire un objet 3d).Je n'ai pas ajouté de support et bien préciser le type de filament dans le slicer.Après avoir mis le G-code dans une carte SD et mis la carte SD dans l'imprimante, j'ai premièrement imprimé la pièce que j avais modélisée avec que la pièce FlexLinks dont je me suis inspiré. J'avais malencontreusement fais l'erreur de dire dans mon code que le diamètre des trous était égale au rayon des tenons Lego, on peut donc voir sur la photo **ci-dessous** que les premières impressions sont trop petite.La deuxième impression était correcte mais les Lego rentraient beaucoup trop dificilement dans ma pièce( voir pas du tout), j ai donc élargis les trous de 0.2mm et j en ai profité pour réduire la taille des "ovales principaux" qui prenaient trop de place.Une erreur d'impression a cassé la troisième tentative mais la quatrième est parfaite, les Lego s'emboitent bien dedans.Les rectifications étaient relativement facile du a la paramétrisation de ma pièce.
![](images/photoDifférentesImpressionsCOMPRIMAX.jpg)
![](images/PhotoBonneFlexLinkCompréssée.jpg)

## Au final...

J'ai appris à calibrer manuellement une imprimente 3D en 5 points sur l'aire d'impression (4 coins + centre) en faisant en sorte que la distance entre la buse et la surface soit égale a l'épaisseur dune feuille de papier en ces 5 points, ce que les imprimentes du fablab font toutes seules. Je connais toute la démarche pour imprimer une pièces: obtenir le fichier STL via le slicer, le mettre sur cart sd,nétoyer le plateau avec du solvant, calibrer la distance entre l'air d'impression et la buse, faire chauffer l'air d'impression et la buse, vérifier qu'il n y à pas de noeud dans la bobine, démarer l'impression et décoller délicatement le pièce de la surface.Il est aussi important de placer sa pièce sur sa plus grande surface sur le plateau.




