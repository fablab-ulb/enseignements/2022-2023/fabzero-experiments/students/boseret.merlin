# 1. Gestion de projet et documentation

L'objectif de ce module est de comprendre comment documenter tout ce que nous allons réaliser lors de ce cour.La documentation est importante car elle permet de garder une trace écrite de nos réalisation. Les personnes interresée par nos travaux pourront voir nos différentes manière d'approcher nos objectif. Cela peut aussi être pratique de manière plus personelle, par exemple dans le cas ou je voudrais revenir sur un ancien travail. Il est important de documenter les réussites commes les échecs.


## Linux

J'ai commencer par installer le shell Linux sur Windows10 [Cette vidéo](https://www.youtube.com/watch?v=CyG16N3GJWo&t=379s&ab_channel=Korben) explique pas a pas le processus. Lorsque'on travaille avec un terminal il n'y a pas de **GUI** (Graphical User Interface), il n'y a que un terminal (Bash) dans lequelle on peut écrire des commandes. [Ce site](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html) répertotrie les commandes basiques.**A noter que les majuscules sont prises en compte** 

| Command |  Fonction   | 
|-----|-----------------|
| cd   | permet de changer de directory ex: [cd home]| permet de se déplacer dans le directory "home" 
| mkdir  | créé un directory ex:[mkdir AH] permet de créé un directory s'appellant AH   |  
| Pwd | Permetde savoir ou on se situe dans les directory  | 

 Une comande que je trouve très pratique si on désire avoir une **GUI** est :

 ```
xfce4-session
 ```

Il faut utiliser cette commande après avoir lancer un serveurX (chez moi ca ne fonctionne que en fullscreen).Cette commande permet d'avoir une véritable interface avec un burreau, de voir ses directory, programmes etc...


![](images/CaptureBureauLinuxCOMPRESSAX.png)

## Git
Nous allons utiliser Gitlab pour mettre notre documentation et site personnel à jour durant toute l'année.Une fois installer il configurer son compte graces aux commandes:
 ```
git config --global user.name "nom d'utilisateur"

git config --global user.mail "adresse mail"
```
Il faut esnuite générer un clef SSH, il s'agit d'un protocol d'itentification (sécurity shell).
Pour générer sa clef SSH il fautt dabbord vérifier si on a bien une version au dessus de 6.5 (sinon c est pas asser sécurisé).Pour se faire il faut utiliser la commande:

```
ssh -V
```

Vus que je n'avais pas encore de clef SSH, j'en ai généré une:
```
ssh-keygen -t ed25519 -C "boseret.merlin"
```
Par la suite j'ai cloné avec SSH le "dossier" a mon nom sur le Gitlab du fablab ULB.

Les commandes pour mettre a jours ce que j'ai changer chez moi, mais pas mis a jour sur le serveur Gitlab sont:

```
git add .
git commit -am "nom de la mise a jour"
git push

```

-git add . Rajoute les dernier fichier rajouter, ou enlève ceux que l'on a enlever.  
-git commit -am Permet de créer un "check point" avec le nom de celui ci entre guillemet
-git push Envoie les mise a jour au serveur




## MarkDown
Le MarkDown est un language simple pour écrire et donner des directive comme par exemple **écrire en gras** _en italic_ ou **_les deux_**.Le language serra ensuite "traduit" en HTML par mKdocs. [Ce site](https://docsify.js.org/#/) contient tout ce qui est utile pour l'écriture en MarkDown.

J'ai télécharger l'éditeur de texte "Visual studio Codde".Très pratique et pas repoussant visuellement.
 
 
## GIMP
Pour compresser les images que je veux mettre sur mon site, j'utilise GIMP. Il est important de les compresser pour qu'elles ne soient pas trop volumineuse sur le site. Il suffit d'ouvrire l'image sur GIMP aller dans Image et cliquer sur échelle et taille de l image. Une fenètre comme celle-ci vas s'ouvrire:

![](images/captureGIMP.JPG)

Il faut maintenant réduire la taille de l'image en réduisant la largeur ou la hauteur de celle-ci, mais attention il faut absolument s'assurer que le chainage entre les deux est actif pour que le rapport entre les deux soit respecté. Et voila l'image est maintenant moin lourde. J' essaye de rester entre 100 Ko et 200 Ko.

## Gestion de projet

Différentes manières d'aborder le cour et les travaux que nous allons devoir fournir au cour de ce trimestre nous ont été présentées. Je pense que mon meilleur proffesseur serra l'expérience mais la **régularité** serra un mot d'ordre.  
Le **"Spiral Developement** consiste a se concentrer sur une tâche précise en particulier et de se fixer un nombre d'heures pour la réaliser. A la fin du temp impartis, on aurra une idée du temps de travail restant et déja une ébauche du travaille. Les prochains temps accordé a ce travail permetteront de continuer celui-ci et de l'étoffer, couche après couche de précision en précision pour devenir de plus en plus globale. Il est aussi conseiller de se concentrer sur le temps et pas le travail a faire et de hiérarchisé le travaille a fournir.





